package datapreprocessing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import utility.Logger;

public class UserIdentification {
	List<List<String>> userList = new ArrayList<List<String>>();
	List<List<String>> userAgent = new ArrayList<List<String>>();
	private List<String> logEntries = new ArrayList<String>();


	public UserIdentification() {
	}

	public void identifyUser(List<String> cleanedData) {	
		String ip = " ";
		List<String> user = null;
		for (int i = 0; cleanedData.size() > 0;) {
			ip = cleanedData.get(0).split(" ")[0];
			user = getUserUsingIpAddress(cleanedData, ip);
			if (user == null) {
				continue;
			}
			userList.add(i, user);
			i++;
		}
	}

	public List<String> getUserUsingIpAddress(List<String> cleanedData,
			String ip) {
		List<String> user = new ArrayList<String>();
		for (String log : cleanedData) {
			if (log.contains(ip)) {
				user.add(log);
			}
		}
		if (user.size() == 0) {
			return null;
		}
		for (String log2 : user) {
			cleanedData.remove(log2);
		}
		return user;
	}

	public void identifyUserUsingAgent() {
		for (List<String> ul : userList) {
			while (!(ul.size() == 0)) {
				List<String> hr = userByAgent(ul, cutAgent(ul.get(0)));
				userAgent.add(hr);
			}
		}				
	}

	public List<String> userByAgent(List<String> user, String ag) {
		List<String> res = new ArrayList<String>();
		String a1 = cutAgent(ag);
		for (String log : user) {
			String a2 = cutAgent(log);
			if (a2.equals(a1)) {
				res.add(log);
			}
		}
		if (res.size() == 0) {
			return null;
		}
		for (String log2 : res) {
			user.remove(log2);
		}
		return res;
	}

	public String cutAgent(String log) {
		String[] token = log.split("\"");
		String erg = token[token.length - 1];
		return erg;
	}

	public String cutUrl(String log) {

		String[] token = log.split(" ");
		String url = token[6];

		return url.substring(1);
	}

	public String cutReferrer(String log) {

		String[] token = log.split(" ");
		String referer = token[10];
		referer = referer.substring(1, referer.toCharArray().length - 1);

		return referer;
	}

	public String cutIp(String log) {

		String[] token = log.split(" ");
		String ip = token[0];

		return ip;
	}

	public String cutDayTime(String log) {

		String[] token = log.split(" ");
		String dayTime = token[3];

		dayTime = dayTime.substring(1);

		return dayTime;
	}

	public List<List<String>> getUserList() {

		return userList;
	}

	public void setUserAgent(List<List<String>> userList) {
		this.userList = userList;
	}

	public List<List<String>> getUserAgent() {

		return userAgent;
	}

	public void setUserList(List<List<String>> userList) {
		this.userList = userList;
	}
	
	public void printUserList(){
		
		for(int i2 = 0; i2 < getUserAgent().size(); i2++){
			for(int j2 = 0; j2 < getUserAgent().get(i2).size(); j2++){
				Logger.getInstance().registerUserActionWithFile(getUserAgent().get(i2).get(j2), "Identified_User");
			}
			Logger.getInstance().registerUserActionWithFile("-------------------------------------------------------------------------------", "Identified_User");
		}	
		Logger.getInstance().registerUserActionWithFile("Number of User with IP" + userList.size(),"Identified_User");
		Logger.getInstance().registerUserActionWithFile("Number of User with IP and Agent" + getUserAgent().size(),"Identified_User");
	}
	
	public void readData(){
		try {
//C:\Users\yceutche\Documents\FrameworkDevelopment\Test_Log
			//String logFilePath = "C:\\Users\\yceutche\\Valid_Log_entries.txt";
		//	String logFilePath = "C:\\Users\\yceutche\\Documents\\FrameworkDevelopment\\Test_Log\\Valid_Log_entries.txt";
			
			String logFilePath = "C:\\Users\\yceutche\\Documents\\FrameworkDevelopment\\Test_Log\\WLRed6616.txt";

			int counter = 1;
			BufferedReader in = new BufferedReader(new FileReader(logFilePath));
			String row = null;
			String rowCopy = null;
			while ((row = in.readLine()) != null) {
				
				counter++;
				rowCopy = row;
				
				logEntries.add(rowCopy);
			}			
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			Logger.getInstance().registerUserAction( e.getMessage());
		}
	}
	public List<String> getLogEntries() {
		return logEntries;
	}
	
	public static void main(String[] args) {
		
		UserIdentification ui = new UserIdentification();
		ui.readData();
		ui.identifyUser(ui.getLogEntries());
		ui.identifyUserUsingAgent();
		ui.printUserList();
		
		
	}

}
