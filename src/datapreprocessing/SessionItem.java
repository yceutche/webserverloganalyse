package datapreprocessing;

/**
 * 
 * @author yceutche
 * 
 * @param <T>
 *            item
 */
public class SessionItem<T> {

	private T item;

	public SessionItem(T t) {
		item = t;
	}

	public T getItem() {
		return item;
	}

	public void setItem(T item) {

		this.item = item;
	}
}
