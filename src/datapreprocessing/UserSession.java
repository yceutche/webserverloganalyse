package datapreprocessing;
import java.util.ArrayList;
import java.util.List;
import utility.Logger;

//import utility.Logger;

public class UserSession{
	
	private List<List<String>> userSessions = new ArrayList<List<String>>();
	
	private static final int SESSION_THRESHOLD_TIME_MIN = 4;

	public UserSession(){
	}

	public List<List<List<String>>> getUserSessionUsingTimeOriented(
			List<String> user){
		List<String> days = new ArrayList<String>();
		List<List<String>> allUserDaySessions = new ArrayList<List<String>>();
		List<List<List<String>>> allUserSessions = new ArrayList<List<List<String>>>();
		for (String s : user) {
			String day = s.split(":")[0];// day
			day = day.substring(1); // remove [
			if (days.contains(day)) {
				continue;
			}
			days.add(day);
		}
		List<String> sessions = null;
		for (String d : days) {
			sessions = buildSessionsOnThisDay(d, user);
			allUserDaySessions.add(sessions);
		}
		for (List<String> ss : allUserDaySessions) {
			List<List<String>> tempSS = buildUserSessionsOnThisDays(ss);
			allUserSessions.add(tempSS);
		}		
		return allUserSessions;
	}

	public List<List<String>> buildUserSessionsOnThisDays(List<String> sessions) {
		List<List<String>> result = new ArrayList<List<String>>();
		String str = sessions.get(0);
		List<String> temp = new ArrayList<String>();
		temp.add(str);

		if (sessions.size() == 1) {
			result.add(0, temp);
			return result;
		}

		String[] startTime = str.split(":");
		int j = 0;
		long startTimeMin = Integer.parseInt(startTime[1]) * 60
				+ Integer.parseInt(startTime[2]);// Sart time in min
		int i = 1;
		for (; i < sessions.size(); i++) {
			String s = sessions.get(i);
			String[] time = s.split(":");
			long timeMin = Integer.parseInt(time[1]) * 60
					+ Integer.parseInt(time[2]);// time next entry in min

			if (timeMin > startTimeMin) {

				long timeDiff = timeMin - startTimeMin;
				
				if ((timeDiff / SESSION_THRESHOLD_TIME_MIN) > 1) {
					
					ArrayList<String> dest = new ArrayList<String>(temp);// Clone

					result.add(j, dest);

					startTimeMin = timeMin;

					temp.clear();
					
					temp.add(s);
					j++;

				} else {
					
					if(!checkUrlAlreadyInUserSession(temp,s))
					temp.add(s);

					if (i == (sessions.size() - 1)) {
						result.add(j, temp);
					
					}
				}
			} else {
				
				if(!checkUrlAlreadyInUserSession(temp,s))
				temp.add(s);
				
				if (i == (sessions.size() - 1)) {
					result.add(j, temp);
				
				}

			}

		}

		if (temp.size() == 1) {// when the last element is a new session with
								// only one element
			if(!str.equals(temp.get(0)))
				result.add(j, temp);
		}

		return result;

	}

	public List<String> buildSessionsOnThisDay(String day, List<String> user) {
		List<String> sessions = new ArrayList<String>();
		for (String s : user) {
			if (s.contains(day)) {
				sessions.add(s);
			}
		}
		return sessions;
	}

	public List<List<String>> getUserSessionUsingReferer(List<String> user) {

		return userSessions;

	}
	
	private boolean checkUrlAlreadyInUserSession(List<String> userSession, String log){	
		boolean result = false;
		String url = log.split(" ")[6];		
		for(String s : userSession){			
			String temp= s.split(" ")[6];			
			if(temp.equals(url)){
				result = true;
				break;
			}
		}
		return result;
	}
}
