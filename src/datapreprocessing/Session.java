package datapreprocessing;

import java.util.ArrayList;
import java.util.List;

import utility.Logger;
/**
 * 
 * @author yceutche
 * 
 */
public class Session<T> {

	private List<SessionItem<T>> items = new ArrayList<SessionItem<T>>();
	
	private List<SessionItem<T>> referrerItems = new ArrayList<SessionItem<T>>();
	private String ip;
	private String agent;
	private String time;

	public void addItem(SessionItem<T> item) {
		
		
		try {	
				items.add(item);
			
		} catch (NullPointerException e) {
			
			Logger.getInstance().registerUserAction(e.getMessage());
		} catch (AssertionError e) {
			Logger.getInstance().registerUserAction(e.getMessage());

		}
	}
	
	public void addReferrerItem(SessionItem<T> r_item) {
		try {
			referrerItems.add(r_item);
		} catch (NullPointerException e) {
			Logger.getInstance().registerUserAction( e.getMessage());

		} catch (AssertionError e) {
			Logger.getInstance().registerUserAction(e.getMessage());

		}
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public void removeUrl(SessionItem<T> item) {
		try {
			items.remove(item);
		} catch (NullPointerException e) {
			Logger.getInstance().registerUserAction("ERROR :::" + e.getMessage());

		} catch (AssertionError e) {
			Logger.getInstance().registerUserAction("ERROR Session Line number 86 " + e.getMessage());

		}

	}
	
	public void removeReferrer(SessionItem<T> r_item) {
		try {
			referrerItems.remove(r_item);
		} catch (NullPointerException e) {
			Logger.getInstance().registerUserAction("ERROR :::" + e.getMessage());

		} catch (AssertionError e) {
			Logger.getInstance().registerUserAction("ERROR ::: " + e.getMessage());

		}

	}
	
	
	public String toString() {
		String result = "";
		int index = 0;
		for (SessionItem<T> url : items) {
			result = "(" + url.getItem() + ")";
			index = index + 1;
		}
		return result;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		Session other = (Session) obj;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		return true;
	}

	public List<SessionItem<T>> getItems() {
		return items;
	}
}
