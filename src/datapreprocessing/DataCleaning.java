package datapreprocessing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import utility.Logger;

public class DataCleaning {
	
	private List<String> logEntries = new ArrayList<String>();
	private static final String AUTO_SEARCH_RECOMMANDATION = "search_recommendations?query=";
	private static final String SEARCH_PAGE_URL = "/search/pages?";
	private FileFilter f_filter;
	int counter = 1;

	public DataCleaning() {

		f_filter = new FileFilter() { // File Filter class

			public boolean accept(File f) { 
											

				return f.getName().toLowerCase().endsWith(".log")
						|| f.getName().toLowerCase().endsWith(".txt")
						|| f.isDirectory();
			}

			public String getDescription() { 
				
				return "files (*.log),(*.txt)";
			}

		};

	}

	public void readAndCleanLogFile() {
		try {

			String logFilePath = getLogFilePath();
			BufferedReader in = new BufferedReader(new FileReader(logFilePath));
			String row = null;
			String rowCopy = null;
			while ((row = in.readLine()) != null) {
				
				counter++;
				rowCopy = row;
				if (row.toLowerCase().contains(".js")
						|| row.toLowerCase().contains(".png")|| row.toLowerCase().contains(".jpg")
						|| row.toLowerCase().contains(".jpeg")|| row.toLowerCase().contains(".css")
						|| row.toLowerCase().contains("robot")|| row.toLowerCase().contains("crawler")
						|| row.toLowerCase().contains("post")|| row.toLowerCase().contains(".ico")
						|| row.toLowerCase().contains(" 400 ")|| row.toLowerCase().contains(" 403 ")
						|| row.toLowerCase().contains(" 304 ")|| row.toLowerCase().contains(" 302 ")
						|| row.toLowerCase().contains("googlebot")|| row.toLowerCase().contains("yandex")
						|| row.toLowerCase().contains("bingbot")|| row.toLowerCase().contains("msnbot")
						|| row.toLowerCase().contains("spider")|| row.toLowerCase().contains("yacybot")
						|| row.toLowerCase().contains("bot.php?")|| row.toLowerCase().contains("/bot")
						|| row.toLowerCase().contains("ccbot")|| row.toLowerCase().contains("icjobs/")
						|| row.toLowerCase().contains("crawlinfo/")	|| row.toLowerCase().contains("hivabot")
						|| row.toLowerCase().contains("::1")|| row.toLowerCase().contains("get / http")// empty url
						|| row.toLowerCase().contains(AUTO_SEARCH_RECOMMANDATION)// auto search query recommadation
						|| row.toLowerCase().contains(".gif ")) {

				} else {
					if(row.toLowerCase().contains(" 200 ")){
						
						if(!checkContainsSearchPagesInUrl(row))
							logEntries.add(rowCopy);
					}
				}
			}
			
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			Logger.getInstance().registerUserAction( e.getMessage());
		}
	}

	public List<String> getLogEntries() {
		return logEntries;
	}

	public void setLogEntries(List<String> logEntries) {
		this.logEntries = logEntries;
	}
	

	public int getCounter() {
		return counter;
	}
	
	public String getLogFilePath() {
		int option = -1;

		JFileChooser directory = new JFileChooser();

		directory.setDialogTitle("Open File"); // Title

		// start on the current directory
		directory.setCurrentDirectory(new File("."));

		// set filter data type
		directory.setFileFilter(f_filter);

		// only files are allowed for selection
		directory.setFileSelectionMode(JFileChooser.FILES_ONLY);

		// single file should be selected
		directory.setMultiSelectionEnabled(false);

		option = directory.showOpenDialog(null);

		if (option == JFileChooser.APPROVE_OPTION) {
			File selectedFile = directory.getSelectedFile();
			return selectedFile.getPath();
		}

		return null;
	}
	
	
	public boolean checkContainsSearchPagesInUrl(String log){
		
		String[] token = log.split(" ");
		String url = token[6];
		if(url.toLowerCase().contains(SEARCH_PAGE_URL)){
			return true;
		}
		return false;
	}
	public static void main(String[] args) {
		
		DataCleaning dc = new DataCleaning();
		dc.readAndCleanLogFile();
		Logger.getInstance().registerUserAction("Raw Data Entries Size = " + dc.getCounter());
		Logger.getInstance().registerUserAction("Valid Entries Size = " + dc.getLogEntries().size());
		Logger.getInstance().registerUserAction("Filterd Entries Size = " + (dc.getCounter() - dc.getLogEntries().size()));
		
		for(int i = 0; i<dc.getLogEntries().size(); i++){
		
			Logger.getInstance().registerUserActionWithFile(dc.getLogEntries().get(i), "Valid_Log_entries");
		}
		
	}
}
