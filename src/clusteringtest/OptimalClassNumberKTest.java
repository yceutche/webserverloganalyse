package clusteringtest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import similarity.EuclideanSimilarity;
import similarity.UrlSimilarity;
import view.HistogramFrame;
import view.SSEFrame;
import clustering.KMedoids;
import clustering.OptimalClassNumberK;
import clustering.SSEComputer;
import datapreprocessing.DataCleaning;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;
import datapreprocessing.UserIdentification;
import datapreprocessing.UserSession;

public class OptimalClassNumberKTest {


	
	private static final int NUMBER_OF_POINTS  = 10000;
	private static final int LOWER_LIMIT  = 10;
	private static final int UPPER_LIMIT  = 780;
	
	KMedoids<Double> km;
	List<Session<Double>> sessions;
	List<Session<String>> sessionsString;
	KMedoids<String> kmString;
	UserIdentification ui = new UserIdentification();
	DataCleaning dc = new DataCleaning();
	UserSession us = new UserSession();
	
	double x,y;


	@Before
	public void setUp() throws Exception {
		
		sessions = new ArrayList<Session<Double>>();
		
		for(int i=0;i<NUMBER_OF_POINTS;i++){
			
			x = (UPPER_LIMIT)*Math.random()+LOWER_LIMIT;
			y = (UPPER_LIMIT)*Math.random()+LOWER_LIMIT;
			
			
			Session<Double> ts = new Session<Double>();
			ts.addItem(new SessionItem<Double>(x));
			ts.addItem(new SessionItem<Double>(y));
			
			sessions.add(ts);		
		}
	}

	@After
	public void tearDown() throws Exception {
		

		sessions = null;
	}
	
	@Ignore
	@Test
	public void testComputeOptimalK() {
		
		Map<Integer, Double> ksse;
		SSEFrame sseF = new SSEFrame();		
		SSEComputer<Double> sse = new SSEComputer<Double>();
		km = new KMedoids<Double>();
		OptimalClassNumberK<Double> optimalK = new OptimalClassNumberK<Double>();
		EuclideanSimilarity euSim = new EuclideanSimilarity();

		//compute Optimal K
		optimalK.computeOptimalK(40, km, sse, sessions, 5, euSim);		
		ksse = optimalK.getkAndSSEpair();
		sseF.getSse_view().actualizedWithSSEValues(ksse);

		assertTrue(true);
	}
	
	
	@Test
	public void testComputeOptimalKString() {
		
		
		List<List<List<String>>> sessions = null;
		kmString = new KMedoids<String>();
				
		sessionsString = new ArrayList<Session<String>>();
		
		dc.readAndCleanLogFile();
		ui.identifyUser(dc.getLogEntries());
		ui.identifyUserUsingAgent();	
		
		int k = 0;
		for(int s = 0; s < ui.getUserAgent().size(); s++){
			
			sessions = us.getUserSessionUsingTimeOriented(ui.getUserAgent().get(s));
		
			for (int i = 0; i < sessions.size(); i++) {
				
			
				for (int j = 0; j < sessions.get(i).size(); j++) {
					
					Session<String> temp = new Session<String>(); 
								
					for (k = 0; k < sessions.get(i).get(j).size(); k++) {
					
						temp.addItem(new SessionItem<String>(ui.cutUrl(sessions.get(i).get(j).get(k))));							
						temp.addReferrerItem(new SessionItem<String>(ui.cutReferrer(sessions.get(i).get(j).get(k))));
						
					}
					
					temp.setIp(ui.cutIp(sessions.get(i).get(j).get(0)));
					temp.setAgent(ui.cutAgent(sessions.get(i).get(j).get(0)));
					temp.setTime(ui.cutDayTime(sessions.get(i).get(j).get(0)));
					
					sessionsString.add(temp);
				}
			}	
			
		}		
	

		Map<Integer, Double> ksse;
		SSEFrame sseF = new SSEFrame();	
		//HistogramFrame hisF = new HistogramFrame();
		SSEComputer<String> sse = new SSEComputer<String>();
		OptimalClassNumberK<String> optimalK = new OptimalClassNumberK<String>();
	    UrlSimilarity urlSim = new UrlSimilarity ();

		//compute Optimal K
		optimalK.computeOptimalK(40, kmString, sse, sessionsString, 5, urlSim);	
		//optimalK.computeOptimalKSC(80, kmString, sse, sessionsString, 5, urlSim);
		ksse = optimalK.getkAndSSEpair();
		//ksse = optimalK.getkAndSSEpair2();
		sseF.getSse_view().actualizedWithSSEValues(ksse);
		//hisF.getHistogramFrame().actualizedWithSSEValues(ksse);
		//System.out.println("Session Size == " + sessions.size() + " Optimal K == " + 5);

		assertTrue(true);
	}
	@Ignore
	@Test
	public void testGetkAndSSEpair() {
		fail("Not yet implemented");
	}

}
