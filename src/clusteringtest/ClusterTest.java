package clusteringtest;

import static org.junit.Assert.*;

//import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import similarity.EuclideanSimilarity;
import clustering.Cluster;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;

public class ClusterTest {

	//private Session medoid;
	//private Similarity sim;
	private Session<Double> s1, s2, s3, s4;
	private Cluster<Double> cl;
	private SessionItem<Double> i1,i2,i3,i4;

	@Before
	public void setUp() throws Exception {
		//sim = new Similarity();
		i1 = new SessionItem<Double>(2.0);
		i2 = new SessionItem<Double>(3.0);
		i3 = new SessionItem<Double>(4.0);
		i4 = new SessionItem<Double>(5.0);
		
		s1 = new Session<Double>();
		s2 = new Session<Double>();
		s3 = new Session<Double>();
		s4 = new Session<Double>();
		
		s1.addItem(i1);
		s2.addItem(i2);
		s3.addItem(i3);
		s4.addItem(i4);
		
		
		cl = new Cluster<Double>(new EuclideanSimilarity());
		cl.addSession(s1);
		cl.addSession(s2);
		cl.addSession(s3);
		cl.addSession(s4);
		
		System.out.println(cl.getSessions().size());
		
	}

	@After
	public void tearDown() throws Exception {
		//sim = null;
		s1= null;
		s2= null;
		s3 = null;
		cl = null;
	}

	@Test
	public void testRemoveSession() {
		cl.removeSession(s1);
		//Positive Test
		assertTrue(cl.getSessions().get(0) == s2);
		assertTrue(cl.getSessions().size() == 3);
		//Negative Test
		assertFalse(cl.getSessions().get(0) == s1);
		assertFalse(cl.getSessions().size() == 4);
		//Null Test
		try {
			cl.removeSession(null);
			fail();
		} catch (NullPointerException e) {
			assertTrue(true);
		}catch (AssertionError e) {
			assertTrue(true);
		}
	}

	@Test
	public void testAddSession() {
		cl.addSession(s4); 
		//Positive Test
		assertTrue(cl.getSessions().size() == 4);
		assertTrue(cl.getSessions().get(3) == s4);
		//Negative Test
		assertFalse(cl.getSessions().size() == 2);
		assertFalse(cl.getSessions().get(2) == s4 );
		//Null Test
		
		try {
			cl.addSession(null);
			fail();
		} catch (NullPointerException e) {
			assertTrue(true);
		}catch (AssertionError e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetSessions() {
	
		//Positive Test
		List<Session<Double>> s = cl.getSessions();
		assertTrue(s.size() == 4);
		assertTrue(s.get(0) == s1);
		assertTrue(s.get(1) == s2);
		assertTrue(s.get(2) == s3);
		assertTrue(s.get(3) == s4);

		//Negative Test
		assertFalse(s.size() == 1);
		assertFalse(s.get(0) == s2);
		assertFalse(s.get(1) == s1);
		assertFalse(s.get(2) == s1);
	}

	@Test
	public void testGetMedoid() {
		Session<Double> s = cl.getSessions().get(0);
		//Positive Test
		assertTrue(s == s1);
		//Negative Test
		assertFalse(s == s2);
	}

	@Test
	public void testGetCost() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetInterclusterCost() {
		fail("Not yet implemented");
	}

	@Test
	public void testEqualsObject() {
		fail("Not yet implemented");
	}

	@Test
	public void testSwapSession() {
		// Weiter hier
	}

	@Test
	public void testContains() {
		fail("Not yet implemented");
	}

	@Test
	public void testErrorComputation() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetSSE() {
		fail("Not yet implemented");
	}

}
