package clusteringtest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import similarity.EuclideanSimilarity;
import view.FrameDraw;
import clustering.Cluster;
import clustering.KMedoids;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;
import exception.MyNullPointerException;

public class KMedoidTest {
	List<Cluster<Double>> clusters;
	KMedoids<Double> km;

	private static final int NUMBER_OF_POINTS = 7000;
	private static final int LOWER_LIMIT = 10;
	private static final int UPPER_LIMIT = 780;

	List<Session<Double>> sessions;
	List<Session<String>> sessions2;

	double x, y;

	@Before
	public void setUp() throws Exception {

		sessions = new ArrayList<Session<Double>>();
		sessions2 = new ArrayList<Session<String>>();

		for (int i = 0; i < NUMBER_OF_POINTS; i++) {

			x = (UPPER_LIMIT) * Math.random() + LOWER_LIMIT;
			y = (UPPER_LIMIT) * Math.random() + LOWER_LIMIT;

			Session<Double> ts = new Session<Double>();
			ts.addItem(new SessionItem<Double>(x));
			ts.addItem(new SessionItem<Double>(y));

			sessions.add(ts);

		}

	}

	@After
	public void tearDown() throws Exception {

		sessions = null;
	}

	@Test(expected = MyNullPointerException.class)
	public void testClusterSession() {

		FrameDraw fd = new FrameDraw();

		clusters = new ArrayList<Cluster<Double>>();
		km = new KMedoids<Double>();
		clusters = km.clusterSession(9, sessions, new EuclideanSimilarity());
		// Positive Test

		fd.getDrawView().actualizedWithClusterList(clusters);

		assertTrue(clusters.size() == 20);
		assertTrue(clusters.get(0).getSessions().size() != 0);
		assertTrue(clusters.get(1).getSessions().size() != 0);
		 assertTrue(clusters.get(2).getSessions().size() != 0);
		// Negative Test

		assertFalse(clusters.size() == 4);
		// Null Test
		// clusters = km.clusterSession(2, null, new EuclideanSimilarity() );
		assertNull(clusters);
	}

	
	@Test
	public void testGetClusters() {
		clusters = new ArrayList<Cluster<Double>>();
		km = new KMedoids<Double>();
		clusters = km.clusterSession(6, sessions, new EuclideanSimilarity());
		List<Cluster<Double>> helpVariable = km.getClusters();
		// Positive Test
		assertTrue(helpVariable.size() != 0);
		assertTrue(helpVariable.size() == 4);
		// Negative Test
		assertFalse(helpVariable.size() < 1);
		assertFalse(helpVariable.get(1).getSessions().get(0).getItems().get(0)
				.getItem() == 120.0);
	}

}
