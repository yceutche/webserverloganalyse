package clustering;

import java.util.List;

import datapreprocessing.Session;

public class SSEComputer<T> {
	
	Cluster<T> cl;
	KMedoids<T> km;

	public double computeSSEFor_K(KMedoids<T> km) {
		double result = 0;
		int kMax = km.getClusters().size();
		for (int k = 0; k < kMax; k++) {
			List<Cluster<T>> clusters = km.getClusterforK(k);
			List<Session<T>> medoids = km.getMedoidsforK(k);
			result = computeSSE(clusters, medoids);
		}
		return result;
	}

	public double computeSSE(List<Cluster<T>> clusters, List<Session<T>> medoids) {
		double res = 0;
		int i = 0;
		for (Cluster<T> c : clusters) {
			res += c.computeSSE(medoids.get(i));
			i++;
		}
		return res;
	}
	
	public double computeSSEForkSC(KMedoids<T> km) {
		double result = 0;
		int kMax = km.getClusters().size();
		for (int k = 0; k < kMax; k++) {

			List<Cluster<T>> clusters = km.getClusterforK(k);
			List<Session<T>> medoids = km.getMedoidsforK(k);
			
			result = computeSCB0(clusters, medoids);
		}
		return result;
	}
	
	public double computeSCB0(List<Cluster<T>> clusters, List<Session<T>> medoids) {
		double max = Double.MAX_VALUE;
		double res = 0;
		double resultTemp= 0;
		int i = 0;
		for (Cluster<T> c : clusters) {
			if(!clusters.contains(medoids)){
				resultTemp += (c.computeDistA0(medoids.get(i))/c.getSessions().size());
				if(resultTemp < max){
					 res = resultTemp;
				}
				i++;
			}
		}
		return res;
	}
}
