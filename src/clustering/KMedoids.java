package clustering;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import similarity.EuclideanSimilarity;
import similarity.Similarity;
import similarity.UrlSimilarity;
import utility.Logger;
import view.FrameDraw;
import datapreprocessing.Session;

/**
 * This class compute the Clustering-Partition method K-Medoids: Input K: the
 * number of clusters D: a data set containing n objects Output: A set of k
 * clusters Method: (1) Arbitrary choose k objects from D as representative
 * objects (seeds) (2) Repeat (2.1) Assignment Step (a) Assign each remaining
 * object to the cluster with the nearest representative object (2.2)
 * Optimization Step (a) For each representative object Oj (medoid for Cluster
 * j) (b) Select a non representative object Orandom (neighbor of medoid) (c)
 * Compute the total cost S of swapping representative object Oj with Orandom
 * (d) replace Oj(medoid) with Orandom for the lower total cost S of swapping
 * (2) Until no change
 * 
 * @author Yannick Ceutche
 */
public class KMedoids<T> {

	private List<Cluster<T>> clusters;
//	FrameDraw fd1 = new FrameDraw();
//	FrameDraw fd2; 
	
	// to be added future similarity types
	public static enum SimilarityTypes{	
		EUCLIDEAN,
		URL_SIMILARITY,
	}
    
	public static final SimilarityTypes useSimilarityType = SimilarityTypes.EUCLIDEAN;
	//public static final SimilarityTypes useSimilarityType = SimilarityTypes.URL_SIMILARITY;
	
	/**
	 * Create a instance of K_Medoids
	 */
	public KMedoids() {
	}

	/**
	 * This method classify some list of object by using K-Medoids algorithm for
	 * more details of algorithm see Class description
	 * 
	 * @param k
	 *            a number of Cluster
	 * @return List for cluster(classified Sessions)
	 */
	public List<Cluster<T>> clusterSession(int k, List<Session<T>> sessionList, Similarity<T> sim) {
		
		boolean once = false;
		
		try {
			/** Create a list of cluster that contain the result of clustering */
			clusters = new ArrayList<Cluster<T>>();
			// (1) Arbitrary chose k session as medoid (cluster center)
			int listSize = 0;
			
		
			
			Random random = new Random();
			for (int i = 0; i < k; i++) {
				listSize = sessionList.size() - 1;
				Cluster<T> cl = new Cluster<T>(sim);
				int index = random.nextInt(listSize);
				Session<T> medoid = sessionList.get(index);// take the first k sessions
				// as medoid
				cl.addSession(medoid);
				clusters.add(cl);
				sessionList.remove(medoid);
			}

			// Repeat the two next steps until the assignment hasn't changed

			List<Session<T>> ListWithoutSwap = new ArrayList<Session<T>>();
			List<Cluster<T>> clusterTemp = new ArrayList<Cluster<T>>();
			List<Session<T>> disimilarSessions = new ArrayList<Session<T>>();
			while (true) {
	
				double distance = 0;
				// (2) Assign each remaining object to the cluster with the
				// nearest
				// representative object
				// for each element in the list of list of elements	
				
				
				for (Session<T> session : sessionList) {
					// find the nearest cluster and the cluster containing the
					// item:
					// similarity
					// Cluster nearestCluster = null;
					// Cluster containingCluster = null;
					
					double distanceToNearestCluster = Double.MAX_VALUE;
							
					if(useSimilarityType == SimilarityTypes.URL_SIMILARITY){
						distanceToNearestCluster = UrlSimilarity.TOKEN_MAX_DISSIMILARITY;
					}
					
					Cluster<T> temp = null;
					

					// calculate the distance between cluster medoids and actual
					// element. The element is assign to the cluster with the
					// smallest distance
					
					for (Cluster<T> cluster : clusters) {
						Session<T> tempMedoid = cluster.getSessions().get(0);	
						
						switch (useSimilarityType){	
						
						case EUCLIDEAN:						
							distance = ((EuclideanSimilarity)(sim)).computeSimilarity((Session<Double>)tempMedoid, 
									                           (Session<Double>)session);
							break;
							
						case URL_SIMILARITY:
							distance = ((UrlSimilarity)(sim)).computeSimilarity((Session<String>)tempMedoid, 
			                           (Session<String>)session);
							break;												
						default:
						
						}
			
						if (distance < distanceToNearestCluster) {
							distanceToNearestCluster = distance;
							temp = cluster;
						}
					
						
					} // end for
					
					if( temp != null ){
						temp.addSession(session);
					
					}else{
						disimilarSessions.add(session);
					}
					
				}
				
	
				//first clustering -> not optimal
				if(!once){
//    			fd1.getDrawView().actualizedWithClusterList(clusters);
//				fd1.setTitle("Clustering with start K-medoids not optimal");
				once = true;
				}
				
				
				ListWithoutSwap.clear();
				sessionList.clear();
				clusterTemp.clear();

				boolean swap = false;
				boolean tempV = false;
				List<Session<T>> sList = null;
				for (Cluster<T> cluster : clusters) {
					tempV = newMedoid(cluster);
					swap = swap | tempV;
					// swap if medoid has changed
					if (tempV) {
						sList = cluster.getSessions();
						if (sList.size() > 1) {
							for (int i = 1; i < sList.size(); i++) {

								sessionList.add(sList.get(i));
							 }
						 }
					    cluster.clearSessions();

					   } else { // retain cluster which did not changed
						 clusterTemp.add(cluster);
					}
				}

				if (!swap) {// if the was no swap break and stop
					break;
				}
				// at least one swap put all element without medoid back in the
				// session list
				for (Cluster<T> c : clusterTemp) {
					sList = c.getSessions();
					if (sList.size() > 1) {
						for (int i = 1; i < sList.size(); i++) {
							sessionList.add(sList.get(i));
						}
					}
					c.clearSessions();
				}
				
				if(disimilarSessions.size() > 0){
				
				for (Session<T> sess : disimilarSessions){
					
					sessionList.add(sess);
				}

				disimilarSessions.clear();
			}
				
			}
							
		} catch (NullPointerException e) {
			e.getMessage();
			Logger.getInstance().registerUserAction(e.getMessage());
		}

		//optimal view
//		fd2 = new FrameDraw();
//		fd2.getDrawView().actualizedWithClusterList(clusters);
//		fd2.setTitle("Clustering with optimal K-medoids");
		
		return clusters;

	}

	/**
	 * deliver all medoids contain in a list of cluster by giving number of
	 * containing clusters
	 * 
	 * @param kMax
	 *            cluster number of a list of cluster
	 * @return
	 */
	public List<Session<T>> getMedoidsforK(int kMax) {
		List<Session<T>> res = new ArrayList<Session<T>>();
		for (int i = 0; i < kMax; i++) {
			res.add(clusters.get(i).getSessions().get(0));
		}
		return res;
	}

	/**
	 * deliver a cluster contain in a list of cluster number of a list of
	 * cluster
	 * 
	 * @param kMax
	 *            cluster number of a list of cluster
	 * @return list of cluster
	 */
	public List<Cluster<T>> getClusterforK(int kMax) {

		List<Cluster<T>> res = new ArrayList<Cluster<T>>();

		for (int i = 0; i < kMax; i++) {
			res.add(clusters.get(i));
		}
		return res;
	}

	/**
	 * deliver the result of the Clustering as list of Cluster
	 * 
	 * @return list of cluster
	 */
	public List<Cluster<T>> getClusters() {
		return clusters;
	}
	public boolean newMedoid(Cluster<T> cluster) {

		Session<T> oldMedoid = cluster.getSessions().get(0);
		List<Session<T>> sessionList = cluster.getSessions();
		Session<T> bestMedoid = oldMedoid;
		double minCost = Double.MAX_VALUE;
		double actualCost = 0;
		for (Session<T> s : sessionList) {
			actualCost = cluster.interclusterCost(s, cluster);
			if (actualCost < minCost) {
				minCost = actualCost;
				bestMedoid = s;
			}
		}
		if (bestMedoid.equals(oldMedoid)) {
			return false;
		}
		cluster.swapSession(oldMedoid, bestMedoid);
		return true;
	}
	

}
