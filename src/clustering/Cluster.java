package clustering;

import java.util.ArrayList;
import java.util.List;

import datapreprocessing.Session;
import similarity.EuclideanSimilarity;
import similarity.Similarity;
import similarity.UrlSimilarity;


/**
 * This method represent a Cluster class. a Cluster contain a list of element
 * for exemple point or Session. The name of the representative element of a
 * Cluster is "medoid". The medoid is located in the first element of a Cluster
 * 
 * @author Yannick Ceutche
 * 
 */
public class Cluster<T> {

	private List<Session<T>> sessions = new ArrayList<Session<T>>();
	private Similarity<T> sim;

	/**
	 * This constructor create a cluster with a specify number of element
	 * 
	 * @param clustersize
	 *            of the cluster
	 */
	public Cluster(Similarity<T> sim) {
		this.sim = sim;
	}

	/**
	 * This method delete some element of the cluster
	 * 
	 * @param session
	 *            that will be deleted
	 */
	public void removeSession(Session<T> session) {
		sessions.remove(session);
	}

	/**
	 * This method add some element to the cluster
	 * 
	 * @param element
	 *            that will be add
	 * @return cluster with added element
	 */
	public void addSession(Session<T> session) {
		if (!sessions.contains(session)) {
			sessions.add(session);
		}
	}

	/**
	 * This method deliver all element contain in a cluster
	 * 
	 * @return list of element
	 */
	public List<Session<T>> getSessions() {
		return sessions;
	}
	/**
	 * This method deliver the cost of the cluster. cost: distance between
	 * medoid and remaining element in the cluster
	 * 
	 * @return the cost of the cluster
	 */
	public double computeCost() {
		double result = 0;
		double interClusterCost = 0;
		int numberOfSessionInCluster = this.getSessions().size();
		
		for (int j = 1; j < numberOfSessionInCluster; j++) {
			
			switch(KMedoids.useSimilarityType){
			
			case EUCLIDEAN:
				interClusterCost = ((EuclideanSimilarity)(sim)).computeSimilarity((Session<Double>)sessions.get(0),
						(Session<Double>)sessions.get(j));
				
				result += interClusterCost;
				break;
				
			case URL_SIMILARITY:
				interClusterCost = ((UrlSimilarity)(sim)).computeSimilarity((Session<String>)sessions.get(0),
						(Session<String>)sessions.get(j));
				
				result += interClusterCost;
				break;									
			default:
					
			}
		}
		return result;

	}

	/**
	 * This medoid deliver the cost of the cluster. This is use after change of
	 * medoid with some element in the cluster
	 * 
	 * @param medoid
	 *            of the cluster
	 * @param cluster
	 *            which cost will be compute
	 * @return the cost of the cluster
	 */
	public double interclusterCost(Session<T> medoid,
			Cluster<T> cluster) {
		double result = 0;
		double interClusterCost = 0;
		int numberOfSessionInCluster = cluster.getSessions().size();
		for (int j = 0; j < numberOfSessionInCluster; j++) {
			
			switch(KMedoids.useSimilarityType){
			
			case EUCLIDEAN:
				interClusterCost = ((EuclideanSimilarity)(sim)).computeSimilarity((Session<Double>)medoid, (Session<Double>)cluster
						.getSessions().get(j));
				
				result += interClusterCost;
				break;
				
			case URL_SIMILARITY:
				interClusterCost = ((UrlSimilarity)(sim)).computeSimilarity((Session<String>)medoid, (Session<String>)cluster
						.getSessions().get(j));
				
				result += interClusterCost;
				break;
				
			default:	
			}
			
		}
		return result;
	}

	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Cluster)) {
			return false;
		}

		Cluster<T> that = (Cluster<T>) other;

		// cluster equality check here.
		return this.getSessions().get(0).equals(that.getSessions().get(0));
	}

	/**
	 * this method swap the medoid with some element in the cluster The element
	 * become medoid and the medoid become simple element
	 * 
	 * @param medoid
	 *            that will be swap
	 * @param e
	 *            the element to swap
	 */
	public void swapSession(Session<T> mmedoid, Session<T> e) {
		sessions.remove(e);
		sessions.set(0, e);
		sessions.add(mmedoid);
	}

	/**
	 * this method verify in oder cluster if element already present
	 * 
	 * @param element
	 *            to verify
	 * @return the result of verification (the are 2 alternatives: true or
	 *         false)
	 */
	public boolean contains(Session<T> ses) {
		return sessions.contains(ses);
	}

	/**
	 * This method compute the error inside a cluster Dies passiert nach dem man
	 * das Medoids vertaucht. Es dient dazu Optimale CLusters zu finden.
	 * 
	 * @return der berechnete Fehler
	 */
	public double errorComputation() {
		double result = 0;
		double re = 0;
		for (Session<T> session : sessions) {
			
			switch(KMedoids.useSimilarityType){
			
			case EUCLIDEAN:
				re = ((EuclideanSimilarity)(sim)).computeSimilarity((Session<Double>)sessions.get(0), (Session<Double>)session);
				result += re;
				break;
				
			case URL_SIMILARITY:
				re = ((UrlSimilarity)(sim)).computeSimilarity((Session<String>)sessions.get(0), (Session<String>)session);
				result += re;
				break;
								
			default:		
			}

		}
		return result;

	}

	/**
	 * this method compute the sum of squared errors inside a cluster
	 * 
	 * @param medoids
	 *            list of medoid of clusters(list of Cluster)
	 * @return result of computed error
	 */
	public double computeSSE(Session<T> medoid) {
		double res = 0;
		double erg = 0;
		for (Session<T> se : sessions) {		
			switch(KMedoids.useSimilarityType){			
			case EUCLIDEAN:
			    erg = ((EuclideanSimilarity)(sim)).computeSimilarity((Session<Double>)se, (Session<Double>)medoid);
				res += erg * erg;				
				break;				
			case URL_SIMILARITY:
				erg = ((UrlSimilarity)(sim)).computeSimilarity((Session<String>)se, (Session<String>)medoid);
				res += erg * erg;
				break;						
			default:			
			}		
		}
		return res;
	}
	public void clearSessions(){
		if(sessions.size() <= 1){
			return;
		}
		for(int i = 1; sessions.size()>1; ){
			sessions.remove(i);
		}
	}
	public double computeDistA0q(Cluster<T> cluster, Session<T> s){
		int result = 0;
		for(Session<T> s1: cluster.sessions){
				
			result += (((UrlSimilarity)(sim)).computeSimilarity((Session<String>)s1,(Session<String>)s) / cluster.getSessions().size());			
			
		}
		
		return result;
		
	}
	public double computeDistA0(Session<T> medoid){
		double result = 0;
		for (Session<T> se : sessions) {
			
			switch(KMedoids.useSimilarityType){
			
			case EUCLIDEAN:
				
				break;
				
			case URL_SIMILARITY:
				result += (((UrlSimilarity)(sim)).computeSimilarity((Session<String>)se,(Session<String>)medoid) / sessions.size());
				break;
				
			default:			
			}
			
		}
		return result;
	}
}
