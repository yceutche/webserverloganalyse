package clustering;

import java.util.List;

import similarity.Similarity;
import similarity.UrlSimilarity;
import datapreprocessing.Session;

public class SilhouetteCoefficient<T> {
	
	KMedoids<T> km;
	Cluster<T> cluster;
	
	public SilhouetteCoefficient() {
	}
	
	public double[] computeSilhouetteCoefficients( List<Session<T>> sessions, int kMax) {
		
		double listSC[] = new double[sessions.size() - 2];
		
		UrlSimilarity sim = new UrlSimilarity();

		for (int k = 2; k < kMax; k++) {
			
			km.clusterSession(k, sessions, (Similarity<T>) sim);
			
			listSC[k-2] = silhouetteCoefficient(km.getClusters(), sessions);
		}
		return listSC;
	}
	
	public double silhouetteCoefficient(List<Cluster<T>> clusters, List<Session<T>> sessions){
		double res = 0;
		double db = 0;
	
		for(Session s: sessions){
			double da = distanceObjectToCluster( findClusterByGivenSession(s,clusters), s);
			double maxValue = Double.MAX_VALUE;
			for(Cluster c: clusters){
				if(!c.contains(s)){
						db = distanceObjectToCluster(c, s);
						if(db < maxValue){
							maxValue = db;
					}
				
				}			
				
			}
			double maximum = da > db ? da : db;
			res += (db - da) / maximum; 
		}
		
		return res;
	}

	public double distanceObjectToCluster (Cluster<T> c, Session<T> session){
		int size = 0;
		UrlSimilarity sim = new UrlSimilarity();
		
		if ( c.contains(session) && c.getSessions().size() == 1) {
			return 0;
		}
		double res = 0;
		List<Session<T>> clusterSessions = c.getSessions();
		
		for(Session s: clusterSessions ){
			if(session != s)
			res += sim.computeSimilarity(s, (Session<String>) session) ;
		}
		if(c.contains(session)){
			size = c.getSessions().size() - 1;
		}else{
			size = c.getSessions().size();
		}
		
		return res/size;		
	}
	
	public Cluster findClusterByGivenSession(Session<T> session, List<Cluster<T>> clusters){
		Cluster findCluster = null;
		for(Cluster c: clusters){
			
			if(clusters.contains(session)){
				
				findCluster = c;
				break;
			}
		}
		
		return findCluster;
	}

}
