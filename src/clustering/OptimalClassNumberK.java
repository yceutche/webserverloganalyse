package clustering;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import similarity.Similarity;
import similarity.UrlSimilarity;
import utility.Logger;
import datapreprocessing.DataCleaning;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;
import datapreprocessing.UserIdentification;
import datapreprocessing.UserSession;

/**
 * This Class compute the optimal number K of cluster for a given set of data
 * using Sum of Squered (SSE). 
 * 
 * @author yceutche
 * 
 */
public class OptimalClassNumberK<T> {
	
	int classNumber;
	
	Map<Integer, Double> kAndSSEpair = new HashMap<Integer, Double>();
	Map<Integer, Double> kAndSSEpair2 = new HashMap<Integer, Double>();


	public OptimalClassNumberK() {
	}

	/**
	 * This method compute the optimal number for a set of data
	 * 
	 * @param maxClassNumber
	 *            the maximal number of cluster: for example 5 mean that the
	 *            method perform K_Medoid clustering for k=2 class or cluster,
	 *            K=3 class or cluster and k = 4 class or cluster. Than deliver
	 *            the representative k maybe K = 2.
	 * @param km
	 *            K-Medoids algorithm for perform clustering algorithm
	 * @param sse
	 *            Sum of Squared Errors for compute the optimal K
	 * @param sessions
	 *            SessionIdentification for getting Sessions to cluster (Set of
	 *            data)
	 * @param step
	 *            calculating steps
	 * @param step
	 *            Similarity Type
	 *            
	 * @return the optimal number K
	 */
	public void computeOptimalK(int maxClassNumber, KMedoids<T> km,
			SSEComputer<T> sse, List<Session<T>> sessions, int step, Similarity<T> sim) {		
		List<Session<T>> helpList = new ArrayList<Session<T>>();
		List<Double> resList = new ArrayList<Double>();	
		if (step < 0){ //default step 
			step = 1;  
		}	
		for (int i = 2; i < maxClassNumber; ) {
			for (Session<T> s : sessions){
				helpList.add(s);
			}
			km.clusterSession(i, helpList, sim);
			double result = sse.computeSSEFor_K(km);
			resList.add(result);
			helpList.clear();
			
			kAndSSEpair.put(i, result); //store in hashmap
			
			i = i+step;
		}
	}
	
	
	public void computeOptimalKSC(int maxClassNumber, KMedoids<T> km,
			SSEComputer<T> sse, List<Session<T>> sessions, int step, Similarity<T> sim) {
		
		List<Session<T>> helpList = new ArrayList<Session<T>>();
		List<Double> resList = new ArrayList<Double>();
		
		if (step < 0){ //default step 
			step = 1;  
		}
		
		for (int i = 2; i < maxClassNumber; ) {
			for (Session<T> s : sessions){
				helpList.add(s);
			}
			
			km.clusterSession(i, helpList, sim);
			double result = sse.computeSSEForkSC(km);
			resList.add(result);
			helpList.clear();
			
			kAndSSEpair2.put(i, result); //store in hashmap
			
			i = i+step;
		}
		
	}
	
	

	public int getClassNumber() {
		return classNumber;
	}

	public Map<Integer, Double> getkAndSSEpair() {
		return kAndSSEpair;
	}
	public Map<Integer, Double> getkAndSSEpair2() {
		return kAndSSEpair2;
	}

	
	public static void main(String[] args) {
		
		SSEComputer<Double> se = new SSEComputer<Double>();
		
		UserIdentification ui = new UserIdentification();
		DataCleaning dc = new DataCleaning();
		UserSession us = new UserSession();
		List<List<List<String>>> sessions = null;
		KMedoids<String> km = new KMedoids<String>();
		List<Cluster<String>> clusteredSessions = null;
		String fileName = "All_Cluster";	
				
		List<Session<String>> allSession = new ArrayList<Session<String>>();		
		dc.readAndCleanLogFile();
		
		List<String> logEntries = dc.getLogEntries();
		
//		for(int i = 0; i<logEntries.size(); i++){
//			
//			Logger.getInstance().registerUserActionWithFile(logEntries.get(i), "Valid_Log_entries");
//		}
				
		ui.identifyUser(dc.getLogEntries());
		ui.identifyUserUsingAgent();
	
		
//	for(int i2 = 0; i2 < ui.getUserAgent().size(); i2++){
//		for(int j2 = 1; j2 < ui.getUserAgent().size()-1; j2++){
//			Logger.getInstance().registerUserActionWithFile(ui.getUserAgent().get(i2).get(j2), "Benutzer_User");
//		}
//	}
		
		int k = 0;
		for(int s = 0; s < ui.getUserAgent().size(); s++){
			
			sessions = us.getUserSessionUsingTimeOriented(ui.getUserAgent().get(s));
			
			Logger.getInstance().registerUserAction("-----SESSIONS_START-----");
		
			for (int i = 0; i < sessions.size(); i++) {
				
			
				for (int j = 0; j < sessions.get(i).size(); j++) {
					
					Session<String> temp = new Session<String>(); 
								
					for (k = 0; k < sessions.get(i).get(j).size(); k++) {
					
						temp.addItem(new SessionItem<String>(ui.cutUrl(sessions.get(i).get(j).get(k))));							
						temp.addReferrerItem(new SessionItem<String>(ui.cutReferrer(sessions.get(i).get(j).get(k))));
						
						Logger.getInstance().registerUserAction(sessions.get(i).get(j).get(k));
						
					}
					temp.setIp(ui.cutIp(sessions.get(i).get(j).get(0)));
					temp.setAgent(ui.cutAgent(sessions.get(i).get(j).get(0)));
					temp.setTime(ui.cutDayTime(sessions.get(i).get(j).get(0)));
					
					allSession.add(temp);
				}
			}	

			Logger.getInstance().registerUserAction("-----SESSIONS_END-------------");
			
		}
		Logger.getInstance().registerUserAction("-----------------");
		// do Clustering
		Logger.getInstance().registerUserAction("Session Size " + allSession.size());

		
		clusteredSessions = km.clusterSession(25, allSession, new UrlSimilarity());	
		int kk = 1;
		for(Cluster<String> cluster : clusteredSessions){		
			Logger.getInstance().registerUserActionWithFile("CLUSTER NUMBER = " + kk +" START",  fileName);
			List<Session<String>> ss = cluster.getSessions();			
			for(Session<String> s : ss){
				Logger.getInstance().registerUserActionWithFile("USER IP " + s.getIp(),  fileName);
				List<SessionItem<String>> items = s.getItems();				
				for(SessionItem<String> item : items){
				  Logger.getInstance().registerUserActionWithFile("Urls :: " + item.getItem(),  fileName);
				}		
			}			
			Logger.getInstance().registerUserActionWithFile("CLUSTER NUMBER = " + kk +" END",  fileName);
			kk++;
		}		
	}
	
}
