package similaritytest;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import similarity.EuclideanSimilarity;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;

public class SimilarityTest {

	SessionItem<Double> item1, item2, item3, item4;
	Session<Double> session1, session2, session3, session4;
	EuclideanSimilarity similarity;

	@Before
	public void setUp() throws Exception {
		

		item1 = new SessionItem<Double>(null);
		item2 = new SessionItem<Double>(null);
		item3 = new SessionItem<Double>(null);
		item4 = new SessionItem<Double>(null);

		item1.setItem(2.0);
		item2.setItem(9.0);
		item3.setItem(11.0);
		item4.setItem(3.0);
		
//		List<Double> item = new ArrayList<Double>();
//		item.add(2.0);
//		item.add(9.0);
//		
//
//		List<Double> item2 = new ArrayList<Double>();
//		item2.add(11.0);
//		item2.add(3.0);
//		item2.setItem(item2);
//		
//		List<Double> item3 = new ArrayList<Double>();
//		item3.add(15.0);
//		item3.add(14.0);
//		item3.setItem(item3);
//		
//		List<Double> item4 = new ArrayList<Double>();
//		item4.add(13.0);
//		item4.add(5.0);
//		item1.setItem(item4);
		
		
		
		session1 = new Session<Double>();
		session2 = new Session<Double>();
		session3 = new Session<Double>();
		session4 = new Session<Double>();
		
		
		session1.addItem(item1);
		session1.addItem(item2);
		session2.addItem(item3);
		session2.addItem(item4);

		similarity = new EuclideanSimilarity();

	}

	@After
	public void tearDown() throws Exception {
		similarity = null;
		item1 = null;
		item2 = null;
		item3 = null;
		item4 = null;
		
		session1 = null;
		session2 = null;
		session3 = null;
		session4 = null;
	}

	@Test//(expected = NullPointerException.class)
	public void testComputeSimilarity() {
		
		// Positive test
		double result1 = similarity.computeSimilarity((Session<Double>) session1,(Session<Double>)  session2);
		double result2 = similarity.computeSimilarity((Session<Double>) session2, (Session<Double>) session1);
		
		assertTrue(result1 == result2); 
		
		//Negative Test
		double result3 = similarity.computeSimilarity(session3, session4);
		assertFalse(result1 == result3);
		
		// Null Test
		double result4 = similarity.computeSimilarity(null, null);
		assertTrue(result4 == 0.0);

	}

}
