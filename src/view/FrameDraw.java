package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;

import javax.swing.JFrame;

public class FrameDraw extends JFrame {

	
	private static final long serialVersionUID = 1L;
	private RectDraw draw_view;
	public static final int WINDOW_SIZE = 850;

	/**
	 * @param args
	 */
	
	public FrameDraw(){
		
		super("Clustering using K-Medoids");	
		this.setSize(WINDOW_SIZE, WINDOW_SIZE);
		
		Container content = this.getContentPane();
		content.setBackground(Color.WHITE);
		content.setLayout(new FlowLayout());	
		
		draw_view = new RectDraw();
		
		content.add(draw_view);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public RectDraw getDrawView() {
		return draw_view;
	}
	

}
