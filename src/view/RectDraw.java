package view;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import clustering.Cluster;
import datapreprocessing.Session;



public class RectDraw<T> extends JPanel {
	

	private static final long serialVersionUID = 1L;

	List<List<Session<Double>>> allClusterNsessions = new ArrayList<List<Session<Double>>>();
	
	boolean actualized_event = false;
	
	List<Color> rgbColor = new ArrayList<Color>();
	int rgbColorSize = 10; // default

	public RectDraw(){
		
		initializedRGBcolors();
		rgbColorSize = rgbColor.size();
	}
	public void paint(Graphics g) {

		Graphics2D graphic2 = (Graphics2D) g;	
		graphic2.setColor(Color.BLACK);	
		graphic2.setStroke(new BasicStroke(8, BasicStroke.CAP_SQUARE,
				BasicStroke.JOIN_MITER));
	
	if(actualized_event){	
		
		for(int j = 0; j<allClusterNsessions.size();j++){//allSessions  = Clusters
			
			List<Session<Double>> clusterNsessions = allClusterNsessions.get(j);
			
	
			graphic2.setColor(rgbColor.get(j%rgbColorSize)); // get next RGB Color and set pixel thickness to two
			graphic2.setStroke(new BasicStroke(8, BasicStroke.CAP_SQUARE,
					BasicStroke.JOIN_MITER));
			
			int x1;
			double temp;
			int y1;
			for (Session<Double> s : clusterNsessions){ //all sessions in one cluster
						
				temp= s.getItems().get(0).getItem();
				x1 = (int)temp;
				
				temp= s.getItems().get(1).getItem();
				y1 = (int)temp;
				
				graphic2.drawLine(x1, y1, x1, y1);
			}
			//set medoid					
			temp= clusterNsessions.get(0).getItems().get(0).getItem();
			x1 = (int)temp;
			
			temp= clusterNsessions.get(0).getItems().get(1).getItem();
			y1 = (int)temp;
			
			 graphic2.setStroke(new BasicStroke(10, BasicStroke.CAP_SQUARE,
						BasicStroke.JOIN_MITER));
			 
			graphic2.setColor(Color.BLACK);
			graphic2.drawLine(x1, y1, x1, y1);
		}
		 
	}	
 
}	
	
	@Override
	public Dimension getPreferredSize() {
		
		return new Dimension(FrameDraw.WINDOW_SIZE ,FrameDraw.WINDOW_SIZE); 
	}

	private void initializedRGBcolors(){
		
		rgbColor.add(new Color(51,102,0));
		rgbColor.add(new Color(255,255,0));
		rgbColor.add(new Color(255,0,0));
		rgbColor.add(new Color(255,153,153));
		
		rgbColor.add(new Color(255,128,0));
		rgbColor.add(new Color(0,153,76));
		rgbColor.add(new Color(204,255,229));
		rgbColor.add(new Color(127,0,255));
		
		rgbColor.add(new Color(160,0,200));
		rgbColor.add(new Color(255,51,103));
		rgbColor.add(new Color(255,255,255));
		rgbColor.add(new Color(128,128,128));
		
		rgbColor.add(new Color(25,101,70));
		rgbColor.add(new Color(104,75,51));
		rgbColor.add(new Color(102,51,0));
		rgbColor.add(new Color(102,0,0));
		
		rgbColor.add(new Color(102,178,255));
		rgbColor.add(new Color(255,25,153));
		rgbColor.add(new Color(153,153,255));
		rgbColor.add(new Color(255,51,255));		
		
	}

	public void actualizedWithClusterList(List<Cluster<T>> clusters){
		
		allClusterNsessions.clear();  //clear old clusters
		
		for(Cluster c : clusters){		
			List<Session<Double>> ss = c.getSessions();
			allClusterNsessions.add(ss);
		}
		
		 actualized_event = true;
		 Graphics my_g = this.getGraphics();
		 my_g.dispose(); 	 
		 this.repaint();
		
	}

}
