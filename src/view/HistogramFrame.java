package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;

public class HistogramFrame extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private HistogramDraw histogramPanel;
		
public HistogramFrame(){
		
		super("Clustering Histogram");	
		this.setSize(600, 600);
		
		Container content = this.getContentPane();
		content.setBackground(Color.lightGray);
		content.setLayout(new FlowLayout());	
		
		histogramPanel = new HistogramDraw();
		
		content.add(histogramPanel);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}


public HistogramDraw getHistogramFrame(){
	
	return histogramPanel;
}


public static void main(String[] args) {
	
	HistogramFrame hf = new HistogramFrame();
	Map<Integer, Double> ksse_map = new HashMap<Integer, Double>();
	
	ksse_map.put(2, 4000.456);
	ksse_map.put(10, 100.456);
	ksse_map.put(15, 0.0);
	ksse_map.put(21, 8000.456);
	ksse_map.put(26, 200.456);
	ksse_map.put(31, 10000.456);
	ksse_map.put(35, 0.0);
	ksse_map.put(41, 0.0);
	ksse_map.put(46, 4000.456);
	ksse_map.put(51, 4000.456);
	
	hf.getHistogramFrame().actualizedWithSSEValues(ksse_map);
}

}
