package view;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.Map;

import javax.swing.JPanel;


public class SSEDraw extends JPanel {
	
	Map<Integer, Double> sse_pairs;
	
	boolean actualized_event = true;

	private static final long serialVersionUID = 1L;
	
	
	private static final int axisLabelSpace = 20;
	
	public void paint(Graphics g) {

		Graphics2D graphic2 = (Graphics2D) g;	
		graphic2.setColor(Color.BLACK);	

		if (actualized_event  ){
			
			 RenderingHints rh = new RenderingHints(
		             RenderingHints.KEY_ANTIALIASING,
		             RenderingHints.VALUE_ANTIALIAS_ON);
			graphic2.setRenderingHints(rh);
			
//			graphic2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//                    RenderingHints.VALUE_ANTIALIAS_ON);
			
	        int w = getWidth();
	        int h = getHeight();

	        // Draw ordinate.
	        graphic2.draw(new Line2D.Double(axisLabelSpace, axisLabelSpace, axisLabelSpace, h-axisLabelSpace));

	        // Draw abscissa.
	        graphic2.draw(new Line2D.Double(axisLabelSpace, h-axisLabelSpace, w-axisLabelSpace, h-axisLabelSpace));
	        
	        // Draw labels.
	        Font font = graphic2.getFont();
	        FontRenderContext frc = graphic2.getFontRenderContext();
	        LineMetrics lm = font.getLineMetrics("0", frc);
	        float a =  lm.getAscent();
	        float b = lm.getDescent();
	        float sh = a + b;
	        
	        // Ordinate label.
	        String s = "SSE values";
	        float syAxis = axisLabelSpace + ((h - 2*axisLabelSpace) - s.length()*sh)/2 + lm.getAscent();
	        for(int i = 0; i < s.length(); i++) {
	            String letter = String.valueOf(s.charAt(i));
	            float sw = (float)font.getStringBounds(letter, frc).getWidth();
	            float sxAxis = (axisLabelSpace - sw)/2;
	            graphic2.drawString(letter, sxAxis, syAxis);
	            syAxis += sh;
	        }
	        
	        s = "K clusters";
	        syAxis = h - axisLabelSpace + (axisLabelSpace - sh)/2 + lm.getAscent();
	        float sw = (float)font.getStringBounds(s, frc).getWidth();
	        float sxAxis = (w - sw)/2;
	        graphic2.drawString(s, sxAxis, syAxis);
	        
	        // Draw curve.
	        double xIncrement = (double)(w - 2*axisLabelSpace)/(sse_pairs.size()-1);
	        double scale = (double)(h - 2*axisLabelSpace)/getMax();
	        graphic2.setPaint(Color.black);
	        int k = 2;
	        for(int i = 0; i < sse_pairs.size()-1; i++,k++) {
	        	
	        	if(sse_pairs.get(k) == null){  //get correct K_V Pair
	        		while(true){
	        			k++;
	        			if(sse_pairs.get(k) != null){
	        				break;  
	        			}
	        		}
	        	}        	
	            double x1 = axisLabelSpace + i*xIncrement;
	            double y1 = h - axisLabelSpace - scale*sse_pairs.get(k);
	            
	            //do same for k+1
	            int temp = k+1;
	         	if(sse_pairs.get(temp) == null){  //get correct K_V Pair
	        		while(true){
	        			temp++;
	        			if(sse_pairs.get(temp) != null){
	        				break;  
	        			}
	        		}
	        	}
	            double x2 = axisLabelSpace + (i+1)*xIncrement;
	            double y2 = h - axisLabelSpace - scale*sse_pairs.get(temp);
	            graphic2.draw(new Line2D.Double(x1, y1, x2, y2));
	        }
	        // Mark data points.
	        graphic2.setPaint(Color.red.darker());
	
	        k = 2;
	        for(int i = 0; i < sse_pairs.size(); i++,k++) {
	        	
	        	if(sse_pairs.get(k) == null){  //get correct K_V Pair
	        		while(true){
	        			k++;
	        			if(sse_pairs.get(k) != null){
	        				break;  
	        			}
	        		}
	        	}       	
	            double x = axisLabelSpace + i*xIncrement;
	            double y = h - axisLabelSpace - scale*sse_pairs.get(k);
	            graphic2.fill(new Ellipse2D.Double(x-2, y-2, 4, 4));
	            
	            if(i!= sse_pairs.size()-1 ){ // insert (k,sse) value on the graph without the last value

	            graphic2.drawString(Integer.toString(k), (float)x+3, (float)y-3);
	            }
	        }
	    
		}

	}
	

	
	private double getMax() {
		double max = -Integer.MAX_VALUE;
		for(int i = 2; i < sse_pairs.size(); i++) {
			if(sse_pairs.get(i) != null)
			  if(sse_pairs.get(i) > max)
				 max = sse_pairs.get(i);
		}
		return max;
}

	@Override
	public Dimension getPreferredSize() {
		
		return new Dimension(SSEFrame.SSE_WINDOW_SIZE ,SSEFrame.SSE_WINDOW_SIZE); 
	}
	

	
	public void actualizedWithSSEValues(Map<Integer, Double> sse){
		
		 sse_pairs = sse;
	
		 actualized_event = true;
		 Graphics g = this.getGraphics();
		 g.dispose(); 	 
		 this.repaint();
		
	}

}
