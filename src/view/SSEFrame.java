package view;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;


import javax.swing.JFrame;

public class SSEFrame extends JFrame {
	
	
	private static final long serialVersionUID = 1L;
	private SSEDraw sse_view;
	public static final int SSE_WINDOW_SIZE = 600;

	/**
	 * @param args
	 */
	
	public SSEFrame(){
		
		super("Clustering graph showing SSE to K-Clusters pairs");	
		this.setSize(SSE_WINDOW_SIZE, SSE_WINDOW_SIZE);
		
		Container content = this.getContentPane();
		content.setBackground(Color.WHITE);
		content.setLayout(new FlowLayout());	
		
		sse_view = new SSEDraw();
		
		content.add(sse_view);
		
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	
	public SSEDraw getSse_view() {
		return sse_view;
	}

}
