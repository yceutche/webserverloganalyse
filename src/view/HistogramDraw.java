package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.FontRenderContext;
import java.awt.font.LineMetrics;
import java.awt.geom.Rectangle2D;
import java.util.Map;

import javax.swing.JPanel;

public class HistogramDraw extends JPanel {

	private static final long serialVersionUID = 1L;

	protected static final int MIN_BAR_WIDTH = 2;
    private Map<Integer, Double> ksse_map;
    boolean actualized = false;
    private int xOffset = 20;
    private int yOffset = 20;
    private int PAD = 20;
    private double OptimizedHistogramDisplay = 1.1;
	
    
    public void paint(Graphics g) {
		
            super.paintComponent(g);
            
            if (ksse_map != null && actualized ) {
                int width = getWidth() - 1 - (xOffset * 2);
                int height = getHeight() - 1 - (yOffset * 2);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setColor(Color.DARK_GRAY);
                g2d.drawRect(xOffset, yOffset, width, height);
                
                // Draw labels.
                int w = getWidth();
    	        int h = getHeight();
    	        Font font = g2d.getFont();
    	        FontRenderContext frc = g2d.getFontRenderContext();
    	        LineMetrics lm = font.getLineMetrics("0", frc);
    	        float sh = lm.getAscent() + lm.getDescent();
    	        // Ordinate label.
    	        String s = "SSE values";
    	        float sy = PAD + ((h - 2*PAD) - s.length()*sh)/2 + lm.getAscent();
    	        for(int i = 0; i < s.length(); i++) {
    	            String letter = String.valueOf(s.charAt(i));
    	            float sw = (float)font.getStringBounds(letter, frc).getWidth();
    	            float sx = (PAD - sw)/2;
    	            g2d.drawString(letter, sx, sy);
    	            sy += sh;
    	        } 	       
    	        s = "K clusters";
    	        sy = h - PAD + (PAD - sh)/2 + lm.getAscent();
    	        float sw = (float)font.getStringBounds(s, frc).getWidth();
    	        float sx = (w - sw)/2;
    	        g2d.drawString(s, sx, sy);
                
                
                int barWidth = Math.max(MIN_BAR_WIDTH,
                        (int) Math.floor((float) width
                        / (float) ksse_map.size()));
                
                double maxValue = 0;
                for (Integer key : ksse_map.keySet()) {
                    double value = ksse_map.get(key);
                    maxValue = Math.max(maxValue, value);
                    
                }               
                // multiply Max value so that the histogram and k-values can be displayed
                maxValue = maxValue*OptimizedHistogramDisplay;   
                
                int xPos = xOffset;
                int i=0;
                for (int key = 1; i < ksse_map.size(); i++,key++) {
                	
        	        	if(ksse_map.get(key) == null){  //get correct K_V Pair
        	        		while(true){
        	        			key++;
        	        			if(ksse_map.get(key) != null){
        	        				break;  
        	        			}
        	        		}
        	        	}
        	        		
                    double value = ksse_map.get(key);
                    int barHeight = Math.round(((float) value
                            / (float) maxValue) * height);
                    
                    //set histogram background color
                    g2d.setColor(Color.BLACK);
                    //g2d.setColor(new Color(key, key, key));
                    int yPos = height + yOffset - barHeight;
                    Rectangle2D bar = new Rectangle2D.Float(
                            xPos, yPos, barWidth, barHeight);
                    g2d.fill(bar);
                    g2d.setColor(Color.GRAY);
                    g2d.draw(bar);
                    
                    //set K-cluster value on Histogram
                    g2d.setColor(Color.RED);
                    g2d.drawString(Integer.toString(key), (float)xPos+xOffset, (float)yPos-3);
                    
                    xPos += barWidth;
                    
                }
                g2d.dispose();
            }
        }	

@Override
public Dimension getPreferredSize() {
	
	return new Dimension(600 ,600); 
}


public void actualizedWithSSEValues(Map<Integer, Double> new_ksse){
	
	 ksse_map = new_ksse;
	
	 actualized = true;
	 Graphics my_g = this.getGraphics();
	 my_g.dispose(); 	 
	 this.repaint();
	
   }

}
