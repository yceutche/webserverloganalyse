package datapreprocessingtest;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import datapreprocessing.Session;
import datapreprocessing.SessionItem;

public class SessionTest {
	List<SessionItem<Double>> items;
	SessionItem<Double> item1, item2, item3, item4;
	Session<Double> session;
	@Before
	public void setUp() throws Exception {
		item1 = new SessionItem<Double>(2.0);
		item2 = new SessionItem<Double>(3.0);
		item3 = new SessionItem<Double>(14.0);
		item4 = null;
		session = new Session<Double>();
	}

	@After
	public void tearDown() throws Exception {
		items = null;
		item1 = null;
		item2 = null;
		item3 = null;
		item4 = null;
	}

	@Test
	public void testAddUrl() {
		session.addItem(item1);
		items = session.getItems(); 
		//Positive Test
		assertTrue(session.getItems().size() == 1);
		assertTrue(items.get(0) == item1);

		//Negative Test
		assertFalse(session.getItems().size() == 2);
		assertFalse(items.get(0) == item2);
		//Null Test
		
		try {
			session.addItem(null);
			fail();
		} catch (NullPointerException e) {
			assertTrue(true);
		}catch (AssertionError e) {
			assertTrue(true);
		}
	}

	@Test
	public void testRemoveUrl() {
		session.addItem(item1);
		session.addItem(item2);
		session.addItem(item3);
		items = session.getItems(); 
		//Positive Test
		session.removeUrl(item1);
		assertTrue(session.getItems().size() == 2);
		assertTrue(items.get(0) == item2);

		//Negative Test
		assertFalse(session.getItems().size() == 1);
		assertFalse(items.get(0) == item3);
		//Null Test
		
		try {
			session.removeUrl(null);
			fail();
		} catch (NullPointerException e) {
			assertTrue(true);
		}catch (AssertionError e) {
			assertTrue(true);
		}	
	}
	@Test
	public void testGetUrls() {
		session.addItem(item1);
		session.addItem(item2);
		session.addItem(item3);
		items = session.getItems();
		//Positive Test
		session.getItems();
		assertTrue(session.getItems().size() == 3);
		assertTrue(items.get(0) == item1);
		assertTrue(items.get(1) == item2);
		assertTrue(items.get(2) == item3);

		//Negative Test
		assertFalse(session.getItems().size() == 1);
		assertFalse(items.get(0) == item2);
		assertFalse(items.get(1) == item1);
		assertFalse(items.get(2) == item1);
	}

}
