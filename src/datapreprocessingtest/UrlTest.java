package datapreprocessingtest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import datapreprocessing.SessionItem;

public class UrlTest {
	SessionItem<List<Double>> url, url2;
	List<Double> item, item2 ;
	
	@Before
	public void setUp() throws Exception {
		url = new SessionItem<List<Double>>(null);
		url2 = new SessionItem<List<Double>>(null);
		item = new ArrayList<Double>();
		item.add(2.0);
		item.add(9.0);
		url.setItem(item);
		item2 = new ArrayList<Double>();
	}

	@After
	public void tearDown() throws Exception {
		url = null;
		url2= null;
		item  = null;
		item2 = null;
	}

	@Test
	public void testGetItem() {
		//Positive Test
		assertTrue(url.getItem().get(0) == 2.0);
		//Negative Test
		assertFalse(url.getItem().get(1) == 1.0);
	}

	@Test
	public void testSetItem() {
		item2.add(1.0);
		item2.add(4.0);
		url2.setItem(item2);
		//Positive Test
		assertTrue(url2.getItem().get(0) == 1.0);
		assertTrue(url2.getItem().get(1) == 4.0);
		//Negative Test
		assertFalse(url2.getItem().get(1) == 5.0);
		assertFalse(url2.getItem().get(1) == 8.0);
	}

}
