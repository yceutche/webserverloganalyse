package exception;

public class MyAssertionError extends AssertionError {

	private static final long serialVersionUID = 5769091309819896203L;

	public MyAssertionError(AssertionError a) {
		System.out.println(a.getMessage());
	}

	public MyAssertionError(String s) {
		System.out.println(s);
	}

}
