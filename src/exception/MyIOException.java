package exception;

import java.io.IOException;

public class MyIOException extends IOException {
	private static final long serialVersionUID = 7188323133440202374L;

	public MyIOException(IOException io) {
		System.out.println(io.getMessage());
	}

	public MyIOException(String s) {
		System.out.println(s);
	}

}
