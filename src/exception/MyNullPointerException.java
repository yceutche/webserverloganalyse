package exception;

public class MyNullPointerException extends NullPointerException {
	private static final long serialVersionUID = 9042162395655455678L;

	public MyNullPointerException(NullPointerException e) {
		System.out.println(e.getMessage());
	}

	public MyNullPointerException(String s) {
		System.out.println(s);
	}

}
