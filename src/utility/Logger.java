package utility;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Logger {

	private static Logger instance = null;
	public static final String CURRENT_DATE_FORMAT  = "dd.MM.yyyy HH:mm:ss";
	private SimpleDateFormat dateFormat = null;
	
	private String userHomePath = System.getProperty("user.home");
	private String logFile = userHomePath + "\\Identified_Sessions.txt";
	private String logFileName = null;
	
	private Logger() {
		
	}
	
	public static Logger getInstance(){
		
		if(instance == null){
			instance = new Logger();
		}
		return instance;
	}
	
	private String getActualTime(){
		
		if(dateFormat == null){
			dateFormat = new SimpleDateFormat(CURRENT_DATE_FORMAT);
		}
		
		Calendar calender = Calendar.getInstance();
		return dateFormat.format(calender.getTime());
	}
	
	public void registerUserAction(String userAction){
		try {
			
			BufferedWriter output = new BufferedWriter(new FileWriter(logFile,true));
			String time = getActualTime();
			time = time + " ::: ";
			output.write(time + userAction);
			output.newLine();
			output.close();
			
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
		}
	}

	public void registerUserActionWithFile(String userAction, String FileName){
		try {
			
			if(logFileName == null){
				
				logFileName = userHomePath + "\\" + FileName + ".txt";	
			}
			
			BufferedWriter output = new BufferedWriter(new FileWriter(logFileName,true));
			output.write( userAction);
			output.newLine();
			output.close();
			
		} catch (Exception e) {
			System.out.println("ERROR: " + e.getMessage());
		}
	}
}
