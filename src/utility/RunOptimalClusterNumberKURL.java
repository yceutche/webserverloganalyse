package utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import similarity.UrlSimilarity;
import view.SSEFrame;
import clustering.KMedoids;
import clustering.OptimalClassNumberK;
import clustering.SSEComputer;
import datapreprocessing.DataCleaning;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;
import datapreprocessing.UserIdentification;
import datapreprocessing.UserSession;

public class RunOptimalClusterNumberKURL {
	
	KMedoids<Double> km;
	List<Session<Double>> sessions;
	List<Session<String>> sessionsString;
	KMedoids<String> kmString;
	UserIdentification ui = new UserIdentification();
	DataCleaning dc = new DataCleaning();
	UserSession us = new UserSession();
	
	public void computeOptimalKURL() {
		
		
		List<List<List<String>>> sessions = null;
		kmString = new KMedoids<String>();
				
		sessionsString = new ArrayList<Session<String>>();
		
		dc.readAndCleanLogFile();
		ui.identifyUser(dc.getLogEntries());
		ui.identifyUserUsingAgent();	
		
		int k = 0;
		for(int s = 0; s < ui.getUserAgent().size(); s++){
			
			sessions = us.getUserSessionUsingTimeOriented(ui.getUserAgent().get(s));
		
			for (int i = 0; i < sessions.size(); i++) {
				
			
				for (int j = 0; j < sessions.get(i).size(); j++) {
					
					Session<String> temp = new Session<String>(); 
								
					for (k = 0; k < sessions.get(i).get(j).size(); k++) {
					
						temp.addItem(new SessionItem<String>(ui.cutUrl(sessions.get(i).get(j).get(k))));							
						temp.addReferrerItem(new SessionItem<String>(ui.cutReferrer(sessions.get(i).get(j).get(k))));
						
					}
					
					temp.setIp(ui.cutIp(sessions.get(i).get(j).get(0)));
					temp.setAgent(ui.cutAgent(sessions.get(i).get(j).get(0)));
					temp.setTime(ui.cutDayTime(sessions.get(i).get(j).get(0)));
					
					sessionsString.add(temp);
				}
			}	
			
		}		
	

		Map<Integer, Double> ksse;
		SSEFrame sseF = new SSEFrame();	
		SSEComputer<String> sse = new SSEComputer<String>();
		OptimalClassNumberK<String> optimalK = new OptimalClassNumberK<String>();
	    UrlSimilarity urlSim = new UrlSimilarity ();

		//compute Optimal K
		optimalK.computeOptimalK(50, kmString, sse, sessionsString, 5, urlSim);	
		ksse = optimalK.getkAndSSEpair();
		sseF.getSse_view().actualizedWithSSEValues(ksse);

	}
	public static void main(String[] args) {
		RunOptimalClusterNumberKURL on = new RunOptimalClusterNumberKURL();
		on.computeOptimalKURL();
	}

	
}
