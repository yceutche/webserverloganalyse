package utility;

import java.util.ArrayList;
import java.util.List;

import similarity.EuclideanSimilarity;
import view.FrameDraw;
import clustering.Cluster;
import clustering.KMedoids;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;


public class RunKMedoidEuclidean {
	List<Cluster<Double>> clusters;
	KMedoids<Double> km;

	private static final int NUMBER_OF_POINTS = 7000;
	private static final int LOWER_LIMIT = 10;
	private static final int UPPER_LIMIT = 780;

	List<Session<Double>> sessions;
	List<Session<String>> sessions2;

	double x, y;

	public  RunKMedoidEuclidean(){
		sessions = new ArrayList<Session<Double>>();
		sessions2 = new ArrayList<Session<String>>();
		for (int i = 0; i < NUMBER_OF_POINTS; i++) {
			x = (UPPER_LIMIT) * Math.random() + LOWER_LIMIT;
			y = (UPPER_LIMIT) * Math.random() + LOWER_LIMIT;
			Session<Double> ts = new Session<Double>();
			ts.addItem(new SessionItem<Double>(x));
			ts.addItem(new SessionItem<Double>(y));
			sessions.add(ts);
		}
	}

	public void testClusterSession() {
		FrameDraw fd = new FrameDraw();
		clusters = new ArrayList<Cluster<Double>>();
		km = new KMedoids<Double>();
		clusters = km.clusterSession(9, sessions, new EuclideanSimilarity());
		fd.getDrawView().actualizedWithClusterList(clusters);
	}
	
	public static void main(String[] args) {
		
		RunKMedoidEuclidean kt = new RunKMedoidEuclidean();
			kt.testClusterSession();
		
	}

}
