package utility;



import java.util.ArrayList;
import java.util.List;
import java.util.Map;




import clustering.KMedoids;
import clustering.OptimalClassNumberK;
import clustering.SSEComputer;
import similarity.EuclideanSimilarity;

import view.SSEFrame;
import datapreprocessing.DataCleaning;
import datapreprocessing.Session;
import datapreprocessing.SessionItem;
import datapreprocessing.UserIdentification;
import datapreprocessing.UserSession;

public class RunOptimalClusterNumberK {

	
	private static final int NUMBER_OF_POINTS  = 2000;
	private static final int LOWER_LIMIT  = 10;
	private static final int UPPER_LIMIT  = 780;
	
	KMedoids<Double> km;
	List<Session<Double>> sessions;
	List<Session<String>> sessionsString;
	KMedoids<String> kmString;
	UserIdentification ui = new UserIdentification();
	DataCleaning dc = new DataCleaning();
	UserSession us = new UserSession();
	
	double x,y;



	public  RunOptimalClusterNumberK(){
		
		sessions = new ArrayList<Session<Double>>();
		
		for(int i=0;i<NUMBER_OF_POINTS;i++){
			
			x = (UPPER_LIMIT)*Math.random()+LOWER_LIMIT;
			y = (UPPER_LIMIT)*Math.random()+LOWER_LIMIT;
			
			
			Session<Double> ts = new Session<Double>();
			ts.addItem(new SessionItem<Double>(x));
			ts.addItem(new SessionItem<Double>(y));
			
			sessions.add(ts);		
		}
	}
	
	public void computeOptimalK() {
		
		Map<Integer, Double> ksse;
		SSEFrame sseF = new SSEFrame();		
		SSEComputer<Double> sse = new SSEComputer<Double>();
		km = new KMedoids<Double>();
		OptimalClassNumberK<Double> optimalK = new OptimalClassNumberK<Double>();
		EuclideanSimilarity euSim = new EuclideanSimilarity();
		//compute Optimal K
		optimalK.computeOptimalK(80, km, sse, sessions, 5, euSim);		
		ksse = optimalK.getkAndSSEpair();
		sseF.getSse_view().actualizedWithSSEValues(ksse);
	}
	
	public static void main(String[] args) {
		RunOptimalClusterNumberK rkn = new RunOptimalClusterNumberK();
		rkn.computeOptimalK();
		
	}

}
