package similarity;

import datapreprocessing.Session;

public class EuclideanSimilarity implements Similarity<Double> {

	public EuclideanSimilarity() {
	}

	@Override
	public double computeSimilarity(Session<Double> session1,
			Session<Double> session2) {
		double res = 0;
		if((session1 != null) || (session2 != null)){		
			for (int i = 0; i < session1.getItems().size(); i++) {
				double square = (session1.getItems().get(i).getItem() - session2
						.getItems().get(i).getItem())
						* (session1.getItems().get(i).getItem() - session2
								.getItems().get(i).getItem());
				res += square;
				
			}
		}
		return Math.sqrt(res);
	}
}
