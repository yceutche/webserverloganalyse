package similarity;

import datapreprocessing.Session;
import datapreprocessing.SessionItem;

public class UrlSimilarity implements Similarity<String> {

	public static final double TOKEN_MAX_DISSIMILARITY = 100;
	private static final double RESOLUTION_INCREASE = 100;

	@Override
	public double computeSimilarity(Session<String> session1,			
			Session<String> session2) {		
		double temp = 0;
		int similarityItemCount = 0;
		double similarityResult = 0.0;
		for (SessionItem<String> s1 : session1.getItems()) {
			for (SessionItem<String> s2 : session2.getItems()) {
				temp = computeUrlSimilarity(s1, s2);
				if (temp == TOKEN_MAX_DISSIMILARITY) {
					continue;
				}
				similarityItemCount++;
				similarityResult = similarityResult + temp;
			}
		}
		// Total Dissimilarity 
		if (similarityItemCount == 0)
			return TOKEN_MAX_DISSIMILARITY;// maxUrlLengthS1S2;		
		return similarityResult;
	}

	private double computeUrlSimilarity(SessionItem<String> urlItem1,
			SessionItem<String> urlItem2) {

		double result = 0.0;
		String url1 = urlItem1.getItem();
		String url2 = urlItem2.getItem();
		String[] token1;
		String[] token2;
		double matchesOfUrl = 0;

		if (url1.contains("/")) {

			token1 = url1.split("/");
		} else {
			token1 = new String[1];
			token1[0] = url1;
		}
		if (url2.contains("/")) {

			token2 = url2.split("/");
		} else {
			token2 = new String[1];
			token2[0] = url2;
		}

//		int lengthToken1 = token1.length;
//		int lengthToken2 = token2.length;

		if ( token1.length <= token2.length) {
			for (int i = 0; i < token1.length; i++) {
				if (token1[i].equals(token2[i])) {
					matchesOfUrl++;
				} else {
					break;
				}
			}
		} else {
			for (int i = 0; i < token2.length; i++) {
				if (token1[i].equals(token2[i])) {
					matchesOfUrl++;
				} else {
					break;
				}
			}
		}
		result = Math.min(1,(matchesOfUrl / (Math.max(1,(Math.max(token1.length, token2.length) - 1)))));
		// Invertion and increase range from 0 to 100
		result = (1 - result) * RESOLUTION_INCREASE;
		return result;
	}

}
