package similarity;

import datapreprocessing.Session;


public interface Similarity<T> {
	

	public double computeSimilarity(Session<T> session1, Session<T> session2);


}
